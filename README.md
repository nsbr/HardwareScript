# HardwareScript


## Description

The HardwareScript is a keystroke injection tool.  
It identify itself has a regular usb keyboard and send programmed keystrokes, interpreting a Keyscript file.  
It sends more than 27000 keystroke by minutes or 5000 words, the rate bottleneck is the OS.  

The HardwareScript has been design to look innocuous, just like a usb flash drive.

The HardwareScript can act like a usb drive, it has a magnetic field sensor which trigger a mode switch. In usb drive mode it is a sd card reader.

![HardwareScript_withcase.jpg](https://gitlab.com/nsbr/HardwareScript/-/raw/master/Pictures/HardwareScript_withcase.jpg "HardwareScript")


## Operation

* A sd card must be in the HardwareScript.  
* To put the HardwareScript in usb drive mode, once the HardwareScript is plugged in a host, bring a magnet near the hall-effect sensor.  
* To operate as a keyboard the HardwareScript must be loaded with a keyscript named `0.ks` at the root folder.  
Once loaded with the keyscript, plug it in a computer, it will executes instructions contained in `0.ks`.
* The Keyscript language specification is at the root of this repo.


## About
This is my first major hardware and firmware design.  
Everything has been done from scratch with no library, from the custom PCB to the various protocoles (SPI, SD, FAT32, USB 2.0, USB HID, USB mass storage, SCSI), passing by the dark corners of PIC32 and the 3D model of the case.  
I learned a lot and I'm happy to share it here.
