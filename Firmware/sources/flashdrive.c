/* 
 * File:   flashdrive.c
 * Author: nsbr
 *
 * Created on August 1, 2017, 2:14 AM
 */

#include "HardwareScript.h"
#include "usb.h"

const struct __attribute__((packed)) flashdrive_descriptors Dflashdrive = {
    0x12, 1, 0x200, 0, 0, 0, 64, 301, 9, 0x500, 1, 2, 3, 1,                                                         /* device */
    9, 2, 32, 1, 1, 0, 0x80, 50,                                                                                    /* configuration */
    9, 4, 0, 0, 2, 8, 6, 0x50, 0,                                                                                   /* interface */
    7, 5, 0x81, 2, 64, 0,                                                                                           /* endpoint1I */
    7, 5, 0x01, 2, 64, 0,                                                                                           /* endpoint1O */
    4, 3, 0x0409,                                                                                                   /* string0 */
    22, 3, {'I', 0, 'n', 0, 'f', 0, 'i', 0, 'n', 0, 'i', 0, 't', 0, 'e', 0, ' ', 0, 'B', 0},                        /* string1 */
    20, 3, {'M', 0, 'e', 0, 'g', 0, 'a', 0, 'F', 0, 'l', 0, 'a', 0, 's', 0, 'h', 0},                                /* string2 */
    26, 3, {'6', 0, '6', 0, '8', 0, '8', 0, 'n', 0, 'l', 0, '4', 0, '1', 0, 'r', 0, '8', 0, '9', 0, '8', 0} ,       /* string3 */
};

const struct __attribute__((packed)) inquiry_data inquiry = {
    {0 ,0}, {0, 1}, 0, {2, 0, 0 , 0}, 32, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0},
    {'A','I', 'G', 'D', '2', 'A', '0'}, {'5', '3', '7', '8'}, {'0', '0', '0', '0'}
};

CDS_TRANSFERT bulk = {0};

static struct __attribute__((packed)) sense_data sense = {0x70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10};

struct __attribute__((packed)) read_capacity capacity = {0, 0x00020000};

static u8   dummy[500] = {0};

static void reset_sense(void) {
    sense.RESPONSE_CODE = 0x70;
    sense.VALID = 0;
    sense.B1 = 0;
    sense.SENSE_KEY = 0;
    sense.x = 0;
    sense.ILI = 0;
    sense.EOM = 0;
    sense.FILEMARK = 0;
    sense.INFORMATION = 0;
    sense.ADDLEN = 10;
    sense.CMDSPINF = 0;
    sense.ASC = 0;
    sense.ASCQ = 0;
    sense.FRUC = 0;
    sense.SENSE_KEY_SPEC[0] = 0;
    sense.SENSE_KEY_SPEC[1] = 0;
    sense.SENSE_KEY_SPEC[2] = 0;
}

static void set_csw(CBW *cbw) {
        bulk.csw.dCSWSignature = 0x53425355;
        bulk.csw.dCSWTag = (*cbw).dCBWTag;
        bulk.csw.dCSWDataResidue = (*cbw).dCBWDataTransferLength;
}

static u32  bulk_read(CBW *cbw, u8 mode) {
    static u8   data[515];
    u32         adress = 0;

    if (!mode) {
        adress = (*cbw).CBWCB[2] << 24 | (*cbw).CBWCB[3] << 16 | (*cbw).CBWCB[4] << 8 | (*cbw).CBWCB[5];
        if (sd_readmblock(data, adress, 0) == -1)
            if (sd_readmblock(data, adress, 0) == -1)
                goto STALL;
        bulk.readwrite = 1;
        return ((u32)data + 1);
    }
    else if (bulk.adress > (u32)&data[512]) {
        if (sd_readmblock(data, 0, 1) == -1)
            goto STALL;
        bulk.adress = (u32)data + 1;
    }
    return (1);
    STALL:
        stall(1, 1);
        stall(1, 1);
        sd_readmblock(0, 0, 2);
        bulk.send = 0;
        bulk.csw.dCSWDataResidue = 0;
        bulk.csw.bCSWStatus = 0;
        bulk.readwrite = 0;
        bulk.adress = 0;
        return (0);
}

static u8 bulk_write(BD bd) {
    static u8   block[512];
    static u16  index = 0;
    u8          *buf;
    
    buf = (u8 *)PHY_TO_LOG(bd.BADDRh);
    xmemcpy(&block[index], buf, bd.BCNTh);
    index += bd.BCNTh;
    bulk.csw.dCSWDataResidue -= bd.BCNTh;
    if (index > 511 || !bulk.csw.dCSWDataResidue) {
        if (sd_writeblock(block, bulk.adress, index) == -1)
            if (sd_writeblock(block, bulk.adress, index) == -1)
                goto STALL;
        bulk.adress++;
        index = 0;
    }
    return (1);
    STALL:
        stall(1, 0);
        stall(1, 0);
        bulk.send = 0;
        bulk.csw.dCSWDataResidue = 0;
        bulk.readwrite = 0;
        bulk.csw.bCSWStatus = 0;
        bulk.adress = 0;
        return (0);
}

void        send_cds(const void *packet, u8 mode) {
    if (!mode) {
        if (packet)
            bulk.adress = (u32)packet;
        else
            bulk.adress += bulk.send;
        if (bulk.readwrite)
             if (!bulk_read(0, 1))
                return;
        bulk.send = bulk.csw.dCSWDataResidue > 64 ? 64 : bulk.csw.dCSWDataResidue;
        send_usb((void *)bulk.adress, 1, bulk.send, bulk.dts);
    }
    else  {
        send_usb(&bulk.csw, 1, 13, bulk.dts);
        if (bulk.send && bulk.readwrite)
            sd_readmblock(0, 0, 2);
        bulk.send = 0;
        bulk.csw.dCSWDataResidue = 0;
        bulk.csw.bCSWStatus = 0;
        bulk.readwrite = 0;
        bulk.adress = 0;
    }
    bulk.dts = !bulk.dts;
}

static void isci_command(CBW *cbw) {
    u32 adress;

    set_csw(cbw);
    if ((*cbw).CBWCB[0] == 0x12)
        goto INQUIRY;
    else if ((*cbw).CBWCB[0] == 0x25)
        goto READCAPACITY;
    else if ((*cbw).CBWCB[0] == 0x28)
        goto READ;
    else if ((*cbw).CBWCB[0] == 0x03)
        goto REQUESTSENSE;
    else if ((*cbw).CBWCB[0] == 0x00)
        goto TESTUNITREADY;
    else if ((*cbw).CBWCB[0] == 0x2A)
        goto WRITE;
    else if ((*cbw).CBWCB[0] == 0x1A)
        goto MODESENSE;
    else
        goto UNSUPPCMD;
    INQUIRY:
        send_cds(&inquiry, 0);
        goto END;
    READCAPACITY:
        send_cds(&capacity, 0);
        goto END;
    READ:
        if ((adress = bulk_read(cbw, 0)))
            send_cds((void *)adress, 0);
        goto END;
    REQUESTSENSE:
        send_cds(&sense, 0);
        reset_sense();
        goto END;
    TESTUNITREADY:
        send_cds(0, 1);
        goto END;
    WRITE:
        bulk.adress = (*cbw).CBWCB[2] << 24 | (*cbw).CBWCB[3] << 16 | (*cbw).CBWCB[4] << 8 | (*cbw).CBWCB[5];
        bulk.readwrite = 1;
        goto END;
    MODESENSE:
        goto UNSUPPCMD;
        goto END;
    UNSUPPCMD:
        if (!bulk.csw.dCSWDataResidue) {
            send_cds(0, 1);
            goto END;
        }
        else {
            if((*cbw).bmCBWFlags) 
                send_cds(&dummy, 0);
            else {
                stall(1, 0);
                stall(1, 0);
            }
        }
        goto END;
    END:
        ;
}

static u8   cbw_isvalid(CBW *cbw) {
    if ((*cbw).dCBWSignature != 0x43425355 ||
        (*cbw).bCBWCBLength < 1 || (*cbw).bCBWCBLength > 16 ||
        bulk.send)
        return (0);
    return (1);
}

void        flashdrive_transfert(void) {
    BD          bd;
    CBW         *cbw;

    usb_ready = 1;
    bd = *BD_FIND;
    if (!bulk.readwrite && !u1stat.DIR) {
        cbw = (CBW *)PHY_TO_LOG(bd.BADDRh);
        if (!cbw_isvalid(cbw)) {
            stall(1, 0);
            stall(1, 0);
            stall(1, 1);
            stall(1, 1);
        }
        else
            isci_command(cbw);
        reset_BD(BD_FIND);
    }
    else {
         if (u1stat.DIR) {
            reset_BD(BD_FIND);
            bulk.csw.dCSWDataResidue -= bulk.send;
            if (bulk.csw.dCSWDataResidue)
                send_cds(0, 0);
            else if (bulk.send)
                send_cds(0, 1);
        }
        else {
            bulk_write(bd);
            if (!bulk.csw.dCSWDataResidue)
                send_cds(0, 1);
            reset_BD(BD_FIND);
        }
    }
}
