/* 
 * File:   usb.c
 * Author: nsbr
 *
 * Created on May 17, 2017, 10:58 AM
 */

#include "HardwareScript.h"
#include "usb.h"

volatile BD                 __attribute__((aligned(512))) bdt[8];
volatile u8                 ep0_Oe[64];
volatile u8                 ep0_Oo[64];
volatile u8                 ep1_Oe[64];
volatile u8                 ep1_Oo[64];
volatile u8                 TX0buf[64];
volatile u8                 TX1buf[64];
volatile USTAT              u1stat;
static volatile u8          even_odd0;
static volatile u8          even_odd1;
static volatile C_TRANSFERT controlIN;
volatile union globales     b;

static void usb_reset(void) {
    u8  c;
    volatile u8  *tmp[8];

    U1CONbits.PPBRST = 1;
    IEC1bits.USBIE = 0;
    IFS1bits.USBIF = 0;
    U1EIRbits.BTSEF = 1;
    U1IRbits.TRNIF = 1;
    U1IRbits.TRNIF = 1;
    U1IRbits.TRNIF = 1;
    U1IRbits.TRNIF = 1;
    tmp[0] = ep0_Oe;
    tmp[1] = ep0_Oo;
    tmp[2] = 0;
    tmp[3] = 0;
    tmp[4] = ep1_Oe;
    tmp[5] = ep1_Oo;
    tmp[6] = 0;
    tmp[7] = 0;

    bdt[0].all = 0;
    bdt[1].all = 0;
    bdt[2].all = 0;
    bdt[3].all = 0;
    bdt[4].all = 0;
    bdt[5].all = 0;
    bdt[6].all = 0;
    bdt[7].all = 0;
    for (c = 0; c < 8; c++) {
        bdt[c].BCNT = 64;
        bdt[c].UOWN = c <= 1 || c == 4 || c == 5 ? 1 : 0;
        bdt[c].BADDR = LOG_TO_PHY((u32)tmp[c]);
    }
    U1ADDRbits.DEVADDR = 0;
    b.all = 0;
    even_odd0 = 0;
    even_odd1 = 0;
    if (usb_mode) {
        sd_readmblock(0, 0, 2);
        xmemset((void *)&bulk, 0, 23);
        U1IEbits.SOFIE = 0;
    }
    else {
       key_stage = 0;
       U1IEbits.SOFIE = 1;
    }
    if (usb_ready) {
        usb_ready = 0;
        enumusb_retry = 0;
        ini_retry_timer();
    }
    U1CONbits.PPBRST = 0;
    IEC1bits.USBIE = 1;
}

void usb_wake(void) {
    U1CONbits.RESUME = 1;
    timer_ms(10);
    U1CONbits.RESUME = 0;
}

void  send_usb (volatile const void *packet, u8 edpt, u32 len, u8 dts) {
    BD          *bdTX;
    volatile u8 *even_odd;

    even_odd = edpt == 0 ? &even_odd0 : &even_odd1;
    bdTX = BD_ENT(edpt, 1, *even_odd);
    (*bdTX).low = 0;
    (*bdTX).BADDR  = LOG_TO_PHY((u32)packet);
    (*bdTX).BCNT = len;
    (*bdTX).DTS = dts;
    (*bdTX).UOWN = 1;
    *even_odd = !*even_odd;
}

void stall(u8 edpt, u8 dir) {

    BD          *bdTX;
    volatile u8 *even_odd;

    even_odd = edpt == 0 ? &even_odd0 : &even_odd1;
    bdTX = BD_ENT(edpt, dir, *even_odd);
    (*bdTX).low = 0;
    (*bdTX).BSTALL = 1;
    (*bdTX).UOWN = 1;
    if (dir)
       *even_odd = !*even_odd;
}

void        reset_BD(volatile BD *bdRX) {
    u8  in = 0;

    if ((*bdRX).PID == 0x9)
        in = 1;
    (*bdRX).low = 0;
    (*bdRX).BCNT = 64;
    if (!in)
        (*bdRX).UOWN = 1;
    else
        (*bdRX).UOWN = 0;
}

static void setup(BD bd) {
    SETUP   *setup;
    u16     nbyte;

    controlIN.remain = 0;
    controlIN.adress = 0;
    controlIN.data = 1;
    setup = (SETUP *)PHY_TO_LOG(bd.BADDRh);
    if ((*setup).bmRequestType & 0x80)
        b.control_pending = 1;
     if ((*setup).bmRequestType & 0x20) {
        if ((*setup).bRequest == 1)
            goto GET_REPORT;
        else if ((*setup).bRequest == 10)
            goto SET_IDLE;
        else if ((*setup).bRequest == 0xFE)
            goto GETMAXLUN;
        else if ((*setup).bRequest == 0xFF)
            goto BOMSR;
    } 
    if ((*setup).bRequest == 0)
        goto GET_STATUS;
    else if ((*setup).bRequest == 1)
        goto CLEAR_FEATURE;
    else if ((*setup).bRequest == 5)
        goto SET_ADRESS;
    else if ((*setup).bRequest == 6)
        goto GET_DESCRIPTOR;
    else if ((*setup).bRequest == 8)
        goto GET_CONFIGURATION;
    else if ((*setup).bRequest == 9)
        goto SET_CONFIGURATION;
    else
        goto STALL;
    GET_DESCRIPTOR:
        controlIN.remain = (*setup).wLength;
        nbyte = controlIN.remain <= 64 ? controlIN.remain : 63;
        if ((*setup).wValue >> 8 == 1)
            controlIN.adress = !usb_mode ? &Dkeyboard.device_bLength : &Dflashdrive.device_bLength;
        else if ((*setup).wValue >> 8 == 2)
            controlIN.adress = !usb_mode ? &Dkeyboard.configuration_bLength : &Dflashdrive.configuration_bLength;
        else if ((*setup).wValue >> 8 == 3)
            switch ((*setup).wValue & 0x00FF) {
                case 0:
                    controlIN.adress = !usb_mode ? &Dkeyboard.string0_bLength : &Dflashdrive.string0_bLength;
                    break;
                case 1:
                    controlIN.adress = !usb_mode ? &Dkeyboard.string1_bLength : &Dflashdrive.string1_bLength;
                    break;
                case 2:
                    controlIN.adress = !usb_mode ? &Dkeyboard.string2_bLength : &Dflashdrive.string2_bLength;
                    break;
                case 3:
                    controlIN.adress = !usb_mode ? &Dkeyboard.string3_bLength : &Dflashdrive.string3_bLength;
                    break;
	    }
        else if ((*setup).wValue >> 8 == 0x21)
            controlIN.adress = &Dkeyboard.HID_bLength;
        else if ((*setup).wValue >> 8 == 0x22) {
            controlIN.adress = &Dkeyboard.report_UsagePage;
            nbyte = 45 < (*setup).wLength ? 45 : (*setup).wLength;
        }
        else
            goto STALL;
        send_usb(controlIN.adress, 0, nbyte, 1);
        goto END;
    SET_ADRESS:
        b.addr = (*setup).wValue;
        send_usb(0, 0, 0, 1);
        b.control_pending = 3;
        goto END;
    GET_STATUS:
        if ((*setup).bmRequestType & 0x3 != 0 && (*setup).wIndex != 0)
            goto STALL;
        TX0buf[1] = TX0buf[0] = 0;
        send_usb(&TX0buf, 0, 2, 1);
        goto END;
    SET_CONFIGURATION:
        if ((*setup).wValue == 1) {
            send_usb(0, 0, 0, 1);
            b.control_pending = 4;
        }
        else
            goto STALL;
        goto END;
    GET_CONFIGURATION:
        TX0buf[0] = 1;
        send_usb(TX0buf, 0, 1, 1);
        goto END;
    CLEAR_FEATURE:
        if (((*setup).bmRequestType & 3) == 2 && !(*setup).wValue ) {
            if (usb_mode) {
                bulk.dts = 0;
                bulk.send = 0;
            }
           if (((*setup).wIndex & 0x0F) == 1) {
                U1EP1bits.EPSTALL = 0;
                reset_BD(bdt + 4);
                reset_BD(bdt + 5);
                bdt[6].PID = 0x9;
                bdt[7].PID = 0x9;
                reset_BD(bdt + 6);
                reset_BD(bdt + 7);
                send_cds(0, 1);
            }
            send_usb(0, 0, 0, 1);
        }
        else
            goto STALL;
        goto END;
    GET_REPORT:
        send_usb(&keys.modifiers, 0, 8, 1);
        goto END;
    SET_IDLE:
        b.idlems = ((*setup).wValue >> 8) * 4;
        send_usb(0, 0, 0, 1);
        goto END;
    GETMAXLUN:
        TX0buf[0] = 0;
        send_usb(&TX0buf, 0, (*setup).wLength, 1);
        goto END;        
    BOMSR:
        bulk.send = 0;
        goto END;
    STALL:
        stall(0, 1);
        goto END;
    END:
        U1CONbits.PKTDIS = 0;
}

static void control_transfert(void) {
    BD          bd;

    bd = *BD_FIND;
    reset_BD(BD_FIND);
    if (bd.PID == 0xD)
        setup(bd);
    else if (b.control_pending == 1) {
		if (!bd.BCNTh)
            b.control_pending = 0;
    }
    else if (b.control_pending == 2) {
        if (bd.BCNTh < 64) {
            send_usb(0, 0, 0, 1);
            b.control_pending = 0;
        }
    }
    else if (b.control_pending == 3) {
        U1ADDR = b.addr;
        b.control_pending = 0;
    }
    else if (b.control_pending == 4) {
        b.conf = 1;
        b.control_pending = 0;
    }
}

void        __ISR(_USB_1_VECTOR, IPL4SRS) USB_ISR() {
    u8          i = 0;

    if (U1IRbits.URSTIF) {
        usb_reset();
        return ;
   }
    if (U1IRbits.IDLEIF)
        i |= 2;
    if (U1IRbits.TRNIF)
        i |= 1;
    u1stat.all = U1STAT & 0x000000FC;
    if (U1EIR)
        b.conf = 0 ;
    U1IRbits.TRNIF = 1;        /* clear them all */
    IFS1bits.USBIF = 0;
    if (i & 1) {
        if (u1stat.ENDPT == 0)
            control_transfert();
        else if (u1stat.ENDPT == 1) {
            if (!usb_mode)
                IFS0bits.CS0IF = 1;
            else
                flashdrive_transfert();
        }
    }
    else if (b.conf && !usb_mode) {
        b.conf = 0;
        ini_timer();
    }
}

void ini_usb(void) {
    U1CONbits.USBEN = 0;
    U1PWRCbits.USBPWR = 0;

    U1BDTP1 = (LOG_TO_PHY((u32)bdt) & 0x0000FE00) >> 8;
    U1BDTP2 = (LOG_TO_PHY((u32)bdt) & 0x00FF0000) >> 16;
    U1BDTP3 = (LOG_TO_PHY((u32)bdt) & 0xFF000000) >> 24;

    __builtin_disable_interrupts();
    INTCONbits.MVEC = 1;
    IPC11bits.USBIP = 4;
    IPC11bits.USBIS = 0;
    IEC1bits.USBIE = 1;
    U1IEbits.URSTIE = 1;
    U1IEbits.TRNIE = 1;
    U1OTGIEbits.ACTVIE = 1;
    U1IEbits.IDLEIE = 1;
    U1IEbits.STALLIE = 1;
    U1EP0bits.EPHSHK = 1;
    U1EP0bits.EPRXEN = 1;
    U1EP0bits.EPTXEN = 1;
    U1EP1bits.EPHSHK = 1;
    U1EP1bits.EPRXEN = 1;
    U1EP1bits.EPTXEN = 1;
     __builtin_enable_interrupts();

    U1CONbits.USBEN = 1;
    U1PWRCbits.USBPWR = 1;
}
