/* 
 * File:   libHS.c
 * Author: nsbr
 *
 * Created on June 27, 2017, 8:20 PM
 */

#include "libHS.h"

void    timer_ms(u32 delay) { /* max 107374 */
    u32 ms1;

    ms1 = SYSCLOCK/1000/2;
    while (delay--) {
        _CP0_SET_COUNT(0); 
        while (_CP0_GET_COUNT() < ms1)
            ;
    }
}

void    timer_s(u32 delay) {
    u16 ms100;
    u16 subcnt;

    ms100 = PBCLOCK/256/10;
    T2CONbits.TCKPS = 7;
    T2CONbits.ON = 1;
    while (delay--)
        for (subcnt = 10; subcnt; subcnt--) {
            TMR2 = 0;
            while (TMR2 < ms100)
                ;
        }
    T2CONbits.ON = 0;
}

void    timer_m(u32 delay) {
    u16 ms100;
    u16 subcnt;

    ms100 = PBCLOCK/256/10;
    T2CONbits.TCKPS = 7;
    T2CONbits.ON = 1;
    while (delay--)
        for (subcnt = 600; subcnt; subcnt--) {
            TMR2 = 0;
            while (TMR2 < ms100)
                ;
        }
    T2CONbits.ON = 0;
}

void    xmemset(void *mem, u8 c, u32 n) {
    u8    *t = (char *)mem;
    while (n--)
        *t++ = c;
}

s8     xmemcmp(u8 *x1, u8 *x2, u16 n) {
    u8 d = 0;

    while (n--) {
        d = *x1 - *x2;
        if (d)
            break;
        x1++, x2++;
    }
    return (d);
}

void    xmemcpy(void *dst, void *src, u32 n) {
    char    *d;
    char    *s;

    d = (char *)dst;
    s = (char *)src;

    while(n--)
        *d++ = *s++;
}

s64		xatoi(u8 *nptr) {
	u8 i;
	s64 a;
	s64 q;
	s64 s;

	i = 0;
	a = 0;
	q = 1;
	s = 1;
	while (*nptr) {
		if (*nptr == '\t')
            nptr++;
		else if (*nptr == '\v')
            nptr++;
		else if (*nptr == '\f')
            nptr++;
		else if (*nptr == '\r')
            nptr++;
		else if (*nptr == '\n')
            nptr++;
		else if (*nptr == ' ')
			nptr++;
        else
            break;
	}
	if (*nptr == '-') {
		nptr++;
		s = -s;
	}
	else if (*nptr == '+')
		nptr++;
	while (nptr[i] >= '0' && nptr[i] <= '9')
		i++;
	while (i) {
		i--;
		a += (nptr[i] - '0') * q;
		q *= 10;
	}
	return (a * s);
}