/* 
 * File:   sd_card.c
 * Author: nsbr
 *
 * Created on June 4, 2017, 8:48 PM
 */

#include "HardwareScript.h"

volatile file f0;
volatile file f1;
static u32  fat_addr;
static u32  root_addr;
static u8   sec_per_clu;

/*
 *  1/ COMMON FUNCTIONS
 *  2/ OPERATION
 *  3/ INITIALISATION
 */

/**************************** 1/3   COMMON FUNCTIONS ******************************/

static void spi_send(u8 byte)
{
    SPI2BUF = byte;
    while (!SPI2STATbits.SPIRBF || !SPI2STATbits.SPITBE)
        ;
    if (SPI2BUF)        /* Read SPI2BUF */
        ;
}

static void spi_take(u8 *r, u32 i) {
    while (i) {
        if (SPI2STATbits.SPITBE && !SPI2STATbits.SPIRBF)
            SPI2BUF = 0xFF;
        if (SPI2STATbits.SPIRBF)
                *r++ = SPI2BUF, i--;
    }
    while (SPI2STATbits.SPIBUSY)
        ;
    if (SPI2BUF)        /* Read SPI2BUF */
        ;
}

static void sd_receive(u8 *response, u8 nbyte)
{
    u8  c = 0;
    u8  i = 0;
    u16  timeout;

    *response = 0xFF;
    for (timeout = 10000; timeout; timeout--) {
        if (SPI2STATbits.SPITBE)
            SPI2BUF = 0xFF;
        if (SPI2STATbits.SPIRBF)
            if (!((c = SPI2BUF) & 0x80) || i)
                response[i++] = c;
        if (i == nbyte)
            break;
    }
    while (!SPI2STATbits.SPIRBF || !SPI2STATbits.SPITBE || SPI2STATbits.SPIBUSY)
        ;
    if (SPI2BUF)
        ;
}

static void sd_receivev2(volatile u8 *response, u8 nbyte)
{
    u8  c;
    u16  i = 0;
    u16  timeout;

    *response = 0xFF;
    for (timeout = 100; timeout; timeout--) {
        SPI2BUF = 0xFF;
        while (!SPI2STATbits.SPIRBF || !SPI2STATbits.SPITBE)
            ;
        if (!((c = SPI2BUF) & 0x80) || i)
            response[i++] = c;
        if (i == nbyte)
            break;
    }
}

static void sd_receive_data(u8 *response, u8 nbyte)
{
    u8  c = 0;
    u8  i = 0;
    u16  timeout;

    *response = 0xFF;
    for (timeout = 10000; timeout; timeout--) {
        if (SPI2STATbits.SPITBE)
            SPI2BUF = 0xFF;
        if (SPI2STATbits.SPIRBF)
            if ((c = SPI2BUF) == 0xFE || c == 0xFC || i)
                response[i++] = c;
        if (i == nbyte)
            break;
    }
    while (!SPI2STATbits.SPIRBF || !SPI2STATbits.SPITBE || SPI2STATbits.SPIBUSY)
        ;
    if (SPI2BUF)
        ;
}

static void sd_cmd(u8 cmd, u32 argument, u8 crc) {
    spi_send(0x40 | cmd);
    spi_send(argument >> 24);
    spi_send(argument >> 16);
    spi_send(argument >> 8);
    spi_send(argument);
    spi_send(crc << 1 | 1);
}

static u8  get_crc7(u64 data,  u16 nbit) {
    u8  reg = 0;

    while (nbit--) {
        reg = reg << 1 | data >> nbit & 1;
        if (reg & 0x80)
            reg ^= 0x9;
    }
    nbit = 0;
    while (nbit++ < 7) {
        reg = reg << 1;
        if (reg & 0x80)
            reg ^= 0x9;
    }
    return (reg);
}
static void sd_receive_block(u8 *data) {
    u8  c;
    u32 i = 0;
    s32 t0 = 100000;

    while (1) {
        if (SPI2STATbits.SPITBE && !SPI2STATbits.SPIRBF)
            SPI2BUF = 0xFF;
        if (SPI2STATbits.SPIRBF)
            if (((c = SPI2BUF) == 0xFE) || i)
                data[i++] = c;
        if (i == 515 || !i && t0 <= 0) {
            i = 2;
            break;
        }
        t0 -= t0 ? 1 : 0;
    }
    while (SPI2STATbits.SPIBUSY || !SPI2STATbits.SPITBE)
        ;
    if (SPI2STATbits.SPIRBF)
        if (SPI2BUF)
          ;
    spi_send(0xFF);
}

void        sd_send_block(u8 *buf, u16 len, u8 mode) {
    u32 x = 0;

    SPI2BUF = mode ? 0xFC : 0xFE;
    while (x <= 515) {
        if (SPI2STATbits.SPITBE && !SPI2STATbits.SPIRBF) {
            if (x < len)
                SPI2BUF = buf[x];
            else
                SPI2BUF = 0;
            x++;
        }
        if (SPI2STATbits.SPIRBF)
            if (SPI2BUF)
                ;
    }
    while (SPI2STATbits.SPIBUSY)
        ;
    if (SPI2STATbits.SPIRBF)
        if (SPI2BUF)
            ;
}

static s8       wake_sd(void) {
    u8  t0;
    u8  r;

    r = 0xFF;
    for (t0 = 255; t0; t0--) {
        sd_cmd(55,0,0);             /* Next cmd is ACMD */
        sd_receivev2(&r, 1);
        sd_cmd(41,0x40000000, 0);
        sd_receivev2(&r, 1);
        if (!(r & 1))               /* Check the Idle bit */
            break;
    }
    if (r & 1)
        return (-1);
    return (0);
}

static s8     check_busysignal(void) {
    u32 t0;

    for (t0 = 1000000; t0; t0--) {
        SPI2BUF = 0xFF;
        while (!SPI2STATbits.SPIRBF || !SPI2STATbits.SPITBE)
            ;
        if (SPI2BUF)
            break;
    }
    if (!t0)
        return (-1);
    return (0);
}

s8          sd_readblock(volatile u8 *buf, u32 adress) {
    u8 r;

    if (check_busysignal())
        return (-1);
    sd_cmd(17, adress, 0);
    sd_receivev2(&r, 1);
    sd_receive_block((u8 *)buf);
    if (*buf != 0xFE)
        return (-1);
    return (0);
}

s8          sd_writeblock(u8 *buf, u32 adress, u16 len) {
    u8  r;

    if (check_busysignal())
        return (-1);
    sd_cmd(24, adress, 0);
    sd_receivev2(&r, 1);
    sd_send_block(buf, len, 0);
    sd_receivev2(&r, 1);
    return (0);
}

s8          sd_readmblock(u8 *buf, u32 begin, u8 mode) {
    u8  r;

    if (!mode) {
        if (check_busysignal())
            return (-1);
        sd_cmd(18, begin, 0);
        sd_receivev2(&r, 1);
    }
    if (buf) {
        sd_receive_block(buf);
        if (*buf != 0xFE)
            return (-1);
    }
    if (mode == 2) {
        check_busysignal();
        sd_cmd(12, 0, 0);
        sd_receivev2(&r, 1);
    }
    return (0);
}

s8          sd_writemblock(u8 *buf, u32 begin, u16 len, u8 mode) {
    u8  r;

    if (check_busysignal())
        return (-1);
    if (!mode) {
        sd_cmd(25, begin, 0);
        sd_receivev2(&r, 1);
    }
    if (buf) {
        sd_send_block(buf, len, 1);
        sd_receivev2(&r, 1);
    }
    if (mode == 2) {
        spi_send(0xFD);
        spi_send(0xFF);
    }
}


/********************************** 2/3 OPERATION **********************************/

static s8   fat_nextcluster(file *file) {
    u16  o;

    if (sd_readblock((*file).data, fat_addr + ((*file).cluster >> 7))
    < 0)
        return (-1);
    o = ((*file).cluster & 0x7F) * 4 + 1;
    if (((*file).cluster = (*file).data[o] | (*file).data[o+1] << 8 |
    (*file).data[o+2] << 16 | (*file).data[o+3] << 24) == 0xFFFFFFFF)
        return (0);
    (*file).sector = 0;
    (*file).remain -= 512;
    (*file).adress = root_addr + ((*file).cluster - 2) * sec_per_clu;
    if ((sd_readblock((*file).data, (*file).adress)) < 0)
        return (-1);
    if ((*file).data[1] == 0 && (*file).data[2] == 0 &&
    (*file).data[3] == 0 &&  (*file).data[4] == 0)
        o = 1;

    return (1);
}

s8          read_file(file *file) {
    if ((*file).remain <= 0)
        return (0);
    if ((*file).sector == sec_per_clu - 1)
        return (fat_nextcluster(file));
    (*file).adress++;
    (*file).remain -= 512;
    (*file).sector++;
    if ((sd_readblock((*file).data, (*file).adress)) < 0)
        return (-1);
    return(1);
}


/******************************* 3/3 INITIALISATION ********************************/

static s8       fat_setfile(file *file, u8 *finfo) {
    (*file).cluster = finfo[20] << 16 | finfo[21] << 24 |
                      finfo[26]  | finfo[27] << 8;
    (*file).sector = 0;
    (*file).remain = (finfo[28] | finfo[29] << 8 |
                      finfo[30] << 16 | finfo[31] << 24) - 512;
    (*file).adress = root_addr + ((*file).cluster - 2) * sec_per_clu;
    if ((sd_readblock((*file).data, (*file).adress)) < 0)
        return (-1);
    return (0);

}

static s8       fat_set_const(void) {
    u8  r[515];
    u32 bpb_addr;

    if (sd_readblock(r, 0) < 0)
        return (-1);
    bpb_addr = r[455] | r[456] << 8 | r[457] << 16 | r[458] << 24;
    if (sd_readblock(r, bpb_addr) < 0)
        return (-1);
    fat_addr = bpb_addr + (r[15] | r[16] << 8);
    root_addr = fat_addr + r[17] * (r[37] | r[38] << 8 | r[39] << 16 | r[40] << 24);
    sec_per_clu = r[14];
    return (0);
}

static s8       fat_searchfiles(void) {
    u8  r[515];
    u16  i;
    u16  j;
    u8  c = 0;

    f0.remain = -1;
    f1.remain = -1;
    if (fat_set_const() < 0)
        return (-1);
    for (j = 0; j < sec_per_clu; j++) {
        if (sd_readblock(r, root_addr + j) < 0)
            return (-1);
        for (i = 1; i < 512 ; i += 32) {
            if (!xmemcmp(r + i, "0       KS ", 11) &&
            !(r[i + 11] & 0x10)) {
                if (fat_setfile(&f0, r + i) < 0)
                    return (-1);
                c++;
            }
            else if (!xmemcmp(r + i, "1       KS ", 11) &&
            !(r[i + 11] & 0x10)) {
                if (fat_setfile(&f1, r + i) < 0)
                    return (-1);
                c++;
            }
            if (c == 2)
                return (0);
        }
    }
    if (!c)
        return (-1);
    return (0);
}

static void     spi_setspeed(void) {
    u8  data[19];
    u32 mspeed;
    u32 speedX[4] = {10, 100, 1000, 100000};
    u8  speedY[16] = {0, 10, 12, 13, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80};
    s16 m;

    sd_cmd(9,0,0);                  /* Get CSD */
    sd_receivev2(data, 1);
    sd_receive_data(data, 19);
    mspeed = speedX[data[4] & 0x3] * speedY[(data[4] & 0x78) >> 3] * 1000;
    if (!mspeed)
        return ;
    SPI2CONbits.ON = 0;
    m = PBCLOCK / (2 * mspeed) - 1;
    if (PBCLOCK / (2 * ((m == -1 ? -2 : m)+ 1)) > mspeed)
        m++;
    if (m < 0)
        m = 0;
    SPI2BRG = m;
    SPI2CONbits.ON = 1;
}

static void     set_capacity(u8 mode) {
    u8  data[19];
    u32 csize;
    s8  csizemult;
    u32 mult;

    sd_cmd(9,0,0);
    sd_receivev2(data, 1);
    sd_receive_data(data, 19);
    if (!mode) {
        csize = (data[7] & 3) << 10 | data[8] << 2 | (data[9] & 0xC0) >> 6;
        csizemult = (data[10] & 3) << 1 | (data[11] & 0x80) >> 7;
        for (mult = 2; csizemult + 2 > 1; csizemult--)
            mult *= 2;
        capacity.lastlba =  (csize + 1) * mult / 512 - 1 ;
    }
    else {
            csize = ((data[8] & 0x3F) << 16 | data[9] << 8 | data[10]);
            capacity.lastlba = (csize + 1) * 1024 - 1;
    }
    capacity.lastlba = REV_WORD(capacity.lastlba);
}

static s8       ini_sdv1(void) {
    u8 r;

    if (wake_sd() == -1)
        return (-1);
    sd_cmd(16, 512, 0);
    sd_receive(&r, 1);
    set_capacity(0);
    spi_setspeed();
    return (0);
}

static s8       ini_sdv2(u8 *response) {
    if  (response[3] != 1 || response[4] != 0x52)
        return (-1);
    if (wake_sd() == -1)
        return (-1);
    sd_cmd(58, 0, 5);               /* Get OCR */
    sd_receive(response, 5);
    if (!(response[1] & 0x40)) {       /* If !CCS it ins't SDHC or SDXC */
        sd_cmd(16, 512, 0);         /* Set Block len to 512 */
        sd_receive(response, 1);
        set_capacity(0);
    }
    else
        set_capacity(1);
    spi_setspeed();
    return (0);
}

static void ini_spi(void) {
    SPI2CONbits.CKP = 0;
    SPI2CONbits.CKE = 1;
    SPI2CONbits.MSTEN = 1;
    SPI2BRG = 55;          /* 357.142 kHz Clock */
    SPI2CONbits.ON = 1;
}

s8        ini_sdcard() {
    s8  i = 0;
    u8  response[5] = {0};
    u8  timeout;

    ini_spi();
    timer_ms(200);
    LATGbits.LATG9 = 1;
    TRISGbits.TRISG9 = 0;
    while (i++ < 10)                  /* Send at least 74 clock pulse */
        spi_send(0xFF);
    LATGbits.LATG9 = 0;
    for (timeout = 50; timeout; timeout--) {
        sd_cmd(0, 0, 0x95 >> 1);
        sd_receive(response, 1);
        if ((*response & 8) == 0)
            break;
    }
    if (!timeout)
        return (-1);
    for (timeout = 50; timeout; timeout--) {
        sd_cmd(8, 0x152, get_crc7(0x152LL | (8LL | 0x40) << 32, 40));
        sd_receive(response, 5);
        if ((*response & 8) == 0)
            break;
    }
    if (*response & 2)                /* Check for Illegal Command */
        i = ini_sdv1();
    else
        i = ini_sdv2(response);
    if (i < 0)
        return (-2);
    else
        return (fat_searchfiles());
}
