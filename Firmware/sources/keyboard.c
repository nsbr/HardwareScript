/* 
 * File:   keyboard.c
 * Author: nsbr
 *
 * Created on July 24, 2017, 10:52 PM
 */

#include "HardwareScript.h"
#include "usb.h"

volatile struct __attribute__((packed)) keys_report         keys;
volatile u8                                                 key_stage = 0;
volatile u16                                                readindex = 1;
volatile jumps                                              jump[JUMPMAX] = {0};
volatile u8                                                 jumpindex = 0; 
volatile u32                                                key_interval = 0; 
const struct __attribute__((packed)) keyboard_descriptors   Dkeyboard = {
    0x12, 1, 0x200, 0, 0, 0, 64, 444, 777, 0x100, 1, 2, 0, 1,                                       /* device */
    9, 2, 34, 1, 1, 0, 0xA0, 50,                                                                    /* configuration */
    9, 4, 0, 0, 1, 3, 0, 1, 0,                                                                      /* interface */
    9, 0x21, 0x111, 33, 1, 0x22, 45,                                                                /* HID */
    7, 5, 0x81, 3, 8, 1,                                                                            /* endpoint1I */
    0x0105, 0x0609, 0x01A1, 0x0705, 0xE019, 0xE729, 0x0015, 0x0125, 0x0175, 0x0895, 0x0281,
    0x0875, 0x0195, 0x0181, 0x0705, 0x0019, 0x6529, 0x0015, 0x6525, 0x0875, 0x0695, 0x0081, 0xC0,   /* report */
    4, 3, 0x0409,                                                                                   /* string0 */
    22, 3, {'I', 0, 'n', 0, 'f', 0, 'i', 0, 'n', 0, 'i', 0, 't', 0, 'e', 0, ' ', 0, 'B', 0},        /* string1 */
    20, 3,  {'K', 0, 'e', 0, 'y', 0, 'b', 0, 'o', 0, 'a', 0, 'r', 0, 'd', 0, '0', 0, },             /* string2 */
    26, 3, {0},                                                                                     /* string3 */
};

static void     convert_key(u8 *key, u8 *mod) {
    if (*key >= 'a' && *key <= 'z')
        *key = *key - 'a' + 4;
    else if (*key >= 'A' && *key <= 'Z') {
        *key = *key - 'A' + 4;
        *mod |= 2;
    }
    else if (*key >= '1' && *key <= '9')
        *key = *key - '1' + 30;
    else if (*key == ' ')
        *key = 44;
    else if (*key == '\t')
        *key = 43;
    else
        switch (*key) {
                                        case '!': *key = 30; *mod |= 2; break;
                                        case '@': *key = 31; *mod |= 2; break;
                                        case '#': *key = 32; *mod |= 2; break;
                                        case '$': *key = 33; *mod |= 2; break;
                                        case '%': *key = 34; *mod |= 2; break;
                                        case '^': *key = 35; *mod |= 2; break;
                                        case '&': *key = 36; *mod |= 2; break;
                                        case '*': *key = 37; *mod |= 2; break;
                                        case '(': *key = 38; *mod |= 2; break;
            case '0': *key = 39; break; case ')': *key = 39; *mod |= 2; break;
            case '-': *key = 45; break; case '_': *key = 45; *mod |= 2; break;
            case '=': *key = 46; break; case '+': *key = 46; *mod |= 2; break;
            case '[': *key = 47; break; case '{': *key = 47; *mod |= 2; break;
            case ']': *key = 48; break; case '}': *key = 48; *mod |= 2; break;
            case '\\': *key = 49; break; case '|': *key = 49; *mod |= 2; break;
            case ';': *key = 51; break; case ':': *key = 51; *mod |= 2; break;
            case '\'': *key = 52; break; case '"': *key = 52; *mod |= 2; break;
                                        case '~': *key = 53; *mod |= 2; break;
            case ',': *key = 54; break; case '<': *key = 54; *mod |= 2; break;
            case '.': *key = 55; break; case '>': *key = 55; *mod |= 2; break;
            case '/': *key = 56; break; case '?': *key = 56; *mod |= 2; break;
            default: *key = 0; break;
        }
}

static void     convert_skey(u8 *word, u8 *key, u8 *mod, u8 size) {
    if (size == 1) {
        if (*word == 'N') *key = 40;
        else if (*word ==  '`') *key = 53;
    }
    else if (size == 2) {
        if (!xmemcmp(word, "LC", 2)) *mod |= 1;
        else if (!xmemcmp(word, "LS", 2)) *mod |= 2;
        else if (!xmemcmp(word, "LA", 2)) *mod |= 4;
        else if (!xmemcmp(word, "LG", 2)) *mod |= 8;
        else if (!xmemcmp(word, "RC", 2)) *mod |= 16;
        else if (!xmemcmp(word, "RS", 2)) *mod |= 32;
        else if (!xmemcmp(word, "RA", 2)) *mod |= 64;
        else if (!xmemcmp(word, "RG", 2)) *mod |= 128;
        else if (!xmemcmp(word, "F1", 2)) *key = 58;
        else if (!xmemcmp(word, "F2", 2)) *key = 59;
        else if (!xmemcmp(word, "F3", 2)) *key = 60;
        else if (!xmemcmp(word, "F4", 2)) *key = 61;
        else if (!xmemcmp(word, "F5", 2)) *key = 62;
        else if (!xmemcmp(word, "F6", 2)) *key = 63;
        else if (!xmemcmp(word, "F7", 2)) *key = 64;
        else if (!xmemcmp(word, "F8", 2)) *key = 65;
        else if (!xmemcmp(word, "F9", 2)) *key = 66;
        else if (!xmemcmp(word, "UP", 2)) *key = 82;
    }
    else if (size == 3) {
        if (!xmemcmp(word, "ESC", 3)) *key = 41;
        else if (!xmemcmp(word, "F10", 3)) *key = 67;
        else if (!xmemcmp(word, "F11", 3)) *key = 68;
        else if (!xmemcmp(word, "F12", 3)) *key = 69;
        else if (!xmemcmp(word, "DEL", 3)) *key = 76;
        else if (!xmemcmp(word, "END", 3)) *key = 77;
        else if (!xmemcmp(word, "F13", 3)) *key = 104;
        else if (!xmemcmp(word, "F14", 3)) *key = 105;
        else if (!xmemcmp(word, "F15", 3)) *key = 106;
        else if (!xmemcmp(word, "F16", 3)) *key = 107;
        else if (!xmemcmp(word, "F17", 3)) *key = 108;
        else if (!xmemcmp(word, "F18", 3)) *key = 109;
        else if (!xmemcmp(word, "F19", 3)) *key = 110;
        else if (!xmemcmp(word, "F20", 3)) *key = 111;
        else if (!xmemcmp(word, "F21", 3)) *key = 112;
        else if (!xmemcmp(word, "F22", 3)) *key = 113;
        else if (!xmemcmp(word, "F23", 3)) *key = 114;
        else if (!xmemcmp(word, "F24", 3)) *key = 115;
        else if (!xmemcmp(word, "CUT", 3)) *key = 123;
    }
    else if (size == 4) {
        if (!xmemcmp(word, "BACK", 4)) *key = 42;
        else if (!xmemcmp(word, "LOCK", 4)) *key = 57;
        else if (!xmemcmp(word, "HOME", 4)) *key = 74;
        else if (!xmemcmp(word, "PGUP", 4)) *key = 75;
        else if (!xmemcmp(word, "LEFT", 4)) *key = 80;
        else if (!xmemcmp(word, "DOWN", 4)) *key = 81;
        else if (!xmemcmp(word, "EXEC", 4)) *key = 116;
        else if (!xmemcmp(word, "HELP", 4)) *key = 117;
        else if (!xmemcmp(word, "MENU", 4)) *key = 118;
        else if (!xmemcmp(word, "STOP", 4)) *key = 120;
        else if (!xmemcmp(word, "UNDO", 4)) *key = 122;
        else if (!xmemcmp(word, "COPY", 4)) *key = 124;
        else if (!xmemcmp(word, "FIND", 4)) *key = 126;
        else if (!xmemcmp(word, "MUTE", 4)) *key = 127;
    }
    else if (size == 5) {
        if (!xmemcmp(word, "PAUSE", 5)) *key = 72;
        else if (!xmemcmp(word, "PGDWN", 5)) *key = 78;
        else if (!xmemcmp(word, "RIGHT", 5)) *key = 79;
        else if (!xmemcmp(word, "SLOCK", 5)) *key = 73;
        else if (!xmemcmp(word, "NLOCK", 5)) *key = 83;
        else if (!xmemcmp(word, "POWER", 5)) *key = 102;
        else if (!xmemcmp(word, "AGAIN", 5)) *key = 121;
        else if (!xmemcmp(word, "PASTE", 5)) *key = 125;
        else if (!xmemcmp(word, "VOLUP", 5)) *key = 128;
    }
    else if (size == 6) {
        if (!xmemcmp(word, "PRINTS", 5)) *key = 70;
        else if (!xmemcmp(word, "INSERT", 6)) *key = 73;
        else if (!xmemcmp(word, "SELECT", 6)) *key = 119;
        else if (!xmemcmp(word, "VOLDWN", 6)) *key = 129;
    }
}

static s16  next_key(void) {
    static u16  treshold;
    u8          key;

    do {
        if (readindex == 1)
            treshold = 512 + ((*f).remain < 0 ? (*f).remain : 0);
        if (readindex > treshold) {
            readindex = 1;
            if (read_file(f) <= 0)
                return (-1);
        }
    } while ((key = (*f).data[readindex++]) == '\n');
    return (key);
}

static s16  get_word(u8 *word) {
    s16 key;
    s16 size = 0;

    BEGIN:
    while ((key = next_key()) != '`') {
        if (key == -1)
            return (-1);
        else
            word[size++] = key;
        if (size > 29)
            size = 2;
    }
    if (size == 0 || size == 1 && key == '~') {
        word[size++] = key;
        goto BEGIN;
    }
    return (size);

}

static void holdrelease(struct keys_report *hold, u8 key, u8 modifier, u8 mode) {
    u8 *k;
    u8 u;

    if (modifier) {
        if (mode == 1)
            (*hold).modifiers |= modifier;
        else
            (*hold).modifiers ^= modifier ;
    }
    else {
        k = (u8 *)&(*hold).key1;
        u = 5;
        if (mode) {
            while (*k != 0 && u) {
               k++;
               u--;
            }
            if (u)
                *k = key;
        }
        else {
            while (*k != key && u) {
               k++;
               u--;
            }
            if (u)
                *k = 0;
        }
    }
}

static  u8 spell(u32 *nb, u8 *word, u8 size) {
    u8 s;
    u8 o;

    if (word[1] == 'L') {
        s = 0;
        o = 6;
    }
    else if (word[1] == 'I') {
        s = 1;
        o = 10;
    }
    else if (word[1] == 'W' && word[6] == 'S') {
        s = 2;
        o = 8;
    }
    else if (word[1] == 'W' && word[5] == 'S') {
        s = 3;
        o = 7;
    }
    else if (word[1] == 'W' && word[5] == 'M') {
        s = 4;
        o = 7;
    }
    word[size] = 0;
    *nb = xatoi(word + o);
    return (s);
}

static void magic_key(u8 *word, u8 size) {
    u8 s;
    u32 nb;


    if ((s = spell(&nb, word, size)) == 0)
        goto LOOP;
    else if (s == 1)
        goto INTERVAL;
    if (s == 2)
        goto WAITMS;
    else if (s == 3)
        goto WAITS;
    else if (s == 4)
        goto WAITM;
    LOOP:
        if (word[size - 1] != 'P') {
                if (jumpindex < JUMPMAX - 1)
                    jumpindex++;
                else
                    goto END;
            jump[jumpindex].f = *f;
            jump[jumpindex].readindex = readindex;
            jump[jumpindex].keys = keys;
            if (nb)
                jump[jumpindex].remain = nb - 1;
            else
                jump[jumpindex].remain = 0xFFFFFFFF;
        }
        else {
            if (!jump[jumpindex].remain) {
                if (jumpindex)
                    jumpindex--;
                goto END;
            }
            *f = jump[jumpindex].f;
            readindex =jump[jumpindex].readindex;
            keys = jump[jumpindex].keys;
            if (jump[jumpindex].remain != 0xFFFFFFFF)
                jump[jumpindex].remain--;
        }
        goto END;

    INTERVAL:
        key_interval = nb;
        goto END;
         
    WAITMS:
        timer_ms(nb);
        goto END;

    WAITS:
        timer_s(nb);
        goto END;

    WAITM:
        timer_m(nb);
        goto END;

    END:
        if (usb_ready)
            IFS0bits.CS0IF = 1;
}

static s8   special_key(u8 *key, u8 *mod, struct keys_report *hold) {
    u8  wrd[30] = {0};
    u8  *word;
    s16 size;
    u8  mode;
    u8  xmod = 0;
    u8  xkey = 0;

    word = wrd;
    if ((size = get_word(word)) == -1)
        return (-1);
    if (word[0] == '~') {
        word++;
        mode = 1;
    }
    else if (word[size - 1] == '~')
        mode = 2;
    else
        mode = 0;
    if (mode) {
        if (size == 2 && *word != 'N' && *word != '`') {
            convert_key(word, &xmod);
            xmod = 0;
            xkey = *word;
        }
        else
            convert_skey(word, &xkey, &xmod, size - 1);
        holdrelease(hold, xkey, xmod, mode);
    }
    else if (*word == '*') {
        magic_key(word, size);
        return (-1);
    }
    else
        convert_skey(word, key, mod, size);
    return (0);
}

s8          update_keys(u8 stage) {
    static struct   keys_report hold = {0};
    u8              mod = 0;
    s16             key = 0;

    if (key_stage) {
        keys.modifiers = hold.modifiers;
        keys.key0 = 0;
        keys.key1 = hold.key1;
        keys.key2 = hold.key2;
        keys.key3 = hold.key3;
        keys.key4 = hold.key4;
        keys.key5 = hold.key5;
        return (0);
    }
    if ((key = next_key()) == -1)
        return (-1);
    if (key == '`') {
        key = 0;
        if (special_key((u8 *)&key, &mod, &hold) == -1)
            return (-1);
    }
    else
        convert_key((u8 *)&key, &mod);
    keys.modifiers = hold.modifiers | mod;
    keys.key0 = key;
    keys.key1 = hold.key1;
    keys.key2 = hold.key2;
    keys.key3 = hold.key3;
    keys.key4 = hold.key4;
    keys.key5 = hold.key5;
    return (0);
}

void        __ISR(_CORE_SOFTWARE_0_VECTOR, IPL4SRS) KEYS_ISR() {
    IFS0bits.CS0IF = 0;
    if (key_stage == 0) {
        if (IFS0bits.INT0IF)
            return ;
        if (update_keys(0) != -1) {
            if (key_interval)
                timer_ms(key_interval);
            send_usb(&keys.modifiers, 1, 8, b.dts);
            b.dts = !b.dts;
            key_stage = 1;
        }
    }
    else if (key_stage == 1) {
        if (update_keys(1) != -1) {
            send_usb(&keys.modifiers, 1, 8, b.dts);
            b.dts = !b.dts;
            key_stage = 0;
        }
    }
}
