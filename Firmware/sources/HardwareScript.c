/* 
 * File:   main.c
 * Author: nsbr
 *
 * Created on May 13, 2017, 9:05 AM
 */

#include "HardwareScript.h"

// DEVCFG3
// USERID = No Setting

// DEVCFG2
// DEVCFG3
// USERID = No Setting

// DEVCFG2
#pragma config FPLLIDIV = DIV_1         // PLL Input Divider (1x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config UPLLIDIV = DIV_1         // USB PLL Input Divider (1x Divider)
#pragma config UPLLEN = ON              // USB PLL Enable (Enabled)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = ON                // Internal/External Switch Over (Enabled)
#pragma config POSCMOD = HS             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_2           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/2)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select (ICE EMUC2/EMUD2 pins shared with PGC2/PGD2)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

volatile file           *f = 0;
volatile u8             usb_ready = 0;
volatile u16            enumusb_retry = 16;
volatile u8             usb_mode = 0;

static void    ini_interrupts() {
    __builtin_disable_interrupts();
    INTCONbits.MVEC = 1;

    TRISDbits.TRISD0 = 1;
    IPC0bits.INT0IP = 4;
    IPC0bits.INT0IS = 2;
    IFS0bits.INT0IF = 0;
    IEC0bits.INT0IE = 1;

    IPC0bits.CS0IP = 4;
    IPC0bits.CS0IS = 3;
    IFS0bits.CS0IF = 0;
    IEC0bits.CS0IE = 1;
   __builtin_enable_interrupts();
}

void        __ISR(_EXTERNAL_0_VECTOR, IPL4AUTO) HE_ISR() {
    IFS0bits.INT0IF = 0;
    IFS0 = 0;
    readindex = 1;
    key_stage = 0;
    jumpindex = 0;
    key_interval = 0;
    usb_ready = 0;
    enumusb_retry = 0;
    if (usb_mode == 1) {
        usb_mode = 0;
        if ((ini_sdcard()) >= 0) {
            if (f0.remain != -1)
                f = &f0;
            else if (f1.remain != -1)
                f = &f1;
        }
        else {
            U1PWRCbits.USBPWR = 0;
            return;
        }
        if ((*f).data[2] == '*' && (*f).data[1] == '`')
            update_keys(0);
    }
    else {
        usb_mode = 1;
        if (ini_sdcard() == -2) {
            U1PWRCbits.USBPWR = 0;
            return;
        }
    }
    ini_retry_timer();
    ini_usb();
}

void ini_timer(void) {
    u32 period;
    __builtin_disable_interrupts();
    T4CONbits.ON = 0;
    T4CONbits.T32 = 1;
    T4CONbits.TCKPS = 0;
    period = PBCLOCK * 3;
    TMR4 = 0;
    TMR5 = 0;
    PR4 = period & 0x0000FFFF; PR5 = period >> 16;
    IPC5bits.T5IP = 4;
    IPC5bits.T5IS = 2;
    IEC0bits.T5IE = 1;
    T4CONbits.ON = 1;
    __builtin_enable_interrupts();
}

void ini_retry_timer (void) {
    __builtin_disable_interrupts();
    IFS0bits.CTIF = 0;
    IPC0bits.CTIP = 4;
    IPC0bits.CTIS = 1;
    IEC0bits.CTIE = 1;
    _CP0_SET_COMPARE(SYSCLOCK/2 * 10);
    _CP0_SET_COUNT(0);
    __builtin_enable_interrupts();
}

void        __ISR(_CORE_TIMER_VECTOR, IPL4AUTO) RETRY_ISR() {
    IFS0bits.CTIF = 0;
    if (!usb_ready && enumusb_retry) {
        enumusb_retry--;
        ini_retry_timer();
        ini_usb();
    }
}

void        __ISR(_TIMER_5_VECTOR, IPL4AUTO) TIMER_ISR() {
    T4CONbits.ON = 0;
    IFS0bits.T5IF = 0;
    usb_ready = 1;
    IFS0bits.CTIF = 1;
    IFS0bits.CS0IF = 1;
}

void    main(void) {
    u8 sd_retry = 16;

    ini_interrupts();
    while (sd_retry)
    {
        if ((ini_sdcard()) >= 0) {
            if (f0.remain != -1)
                f = &f0;
            else if (f1.remain != -1)
                f = &f1;
            break;
        }
        sd_retry--;
    }
    if (!f)
        goto WHILE;
    if ((*f).data[2] == '*' && (*f).data[1] == '`')
        update_keys(0);
    ini_retry_timer();
    ini_usb();
    WHILE:
        while (1)
            ;
}
