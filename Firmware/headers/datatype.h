/* 
 * File:   datatype.h
 * Author: nsbr
 *
 * Created on May 13, 2017, 4:52 PM
 */

#ifndef DATATYPE_H
#define	DATATYPE_H

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

#endif