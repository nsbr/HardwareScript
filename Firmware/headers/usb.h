/* 
 * File:   usb.h
 * Author: nsbr
 *
 * Created on July 9, 2017, 10:46 AM
 */

#ifndef USB_H
#define	USB_H

#define BD_ENT(edpt, dir, ppbi) ((BD *)((u32)bdt + 0x20 * edpt + 0x10 * dir + 0x8 * ppbi))
#define BD_FIND ((BD *)((u32)bdt | ((u1stat.all & 0x000000FC) << 1)))
#define BD_EFIND ((BD *)((u32)bdt | ((u1stat.all & 0x000000F0) << 1)))

typedef union buffer_descriptor {
    struct {
        u64	: 2;
        u64	BSTALL: 1;
        u64	DTSEN : 1;
        u64	NINC : 1;
        u64	KEEP : 1;
        u64	DTS : 1;
        u64	UOWN: 1;
        u64	: 8;
        u64	BCNT : 10;
        u64	: 6;
        u64	BADDR : 32;
    };
    struct {
        u64	: 2;
        u64	PID: 4;
        u64	DTSh : 1;
        u64	UOWNh : 1;
        u64	: 8;
        u64	BCNTh : 10;
        u64	: 6;
        u64	BADDRh : 32;
    };
    struct {
        u64 low : 32;
        u64 high : 32;
    };
    u64 all;
} BD;

typedef union usb_status_register {
    u32 all;
    struct {
        u32 : 2;
        u32 PPBI: 1;
        u32 DIR: 1;
        u32 ENDPT: 4;
    };
} USTAT;

typedef struct setup_packet {
    u8  bmRequestType;
    u8  bRequest;
    u16 wValue;
    u16 wIndex;
    u16 wLength;
} __attribute__((packed)) SETUP;

typedef volatile struct control_transfer {
    s32         remain;
    const void  *adress;
    u8          data;
} C_TRANSFERT;

typedef struct __attribute__((packed)) keyboard_descriptors {
    u8  device_bLength;
    u8  device_bDescriptorType;
    u16 device_bcdUSB;
    u8  device_bDeviceClass;
    u8  device_bDeviceSubClass;
    u8  device_bDeviceProtocol;
    u8  device_bMaxPacketSize0;
    u16 device_idVendor;
    u16 device_idProduct;
    u16 device_bcdDevice;
    u8  device_iManufacturer;
    u8  device_iProduct;
    u8  device_iSerialNumber;
    u8  device_bNumConfigurations;

    u8  configuration_bLength;
    u8  configuration_bDescriptorType;
    u16 configuration_wTotalLength;
    u8  configuration_bNumInterfaces;
    u8  configuration_bConfigurationValue;
    u8  configuration_iConfiguration;
    u8  configuration_bmAttributes;
    u8  configuration_bMaxPower;

    u8  interface_bLength;
    u8  interface_bDescriptorType;
    u8  interface_bInterfaceNumber;
    u8  interface_bAlternateSetting;
    u8  interface_bNumEndpoints;
    u8  interface_bInterfaceClass;
    u8  interface_bInterfaceSubClass;
    u8  interface_bInterfaceProtocol;
    u8  interface_iInterface;

    u8  HID_bLength;
    u8  HID_bDescriptorType;
    u16 HID_bcdHID;
    u8  HID_bCountryCode;
    u8  HID_bNumDescriptors;
    u8  HID_bDescriptorType1;
    u16 HID_wDescriptorLength1;

    u8  endpoint1I_bLength;
    u8  endpoint1I_bDescriptorType;
    u8  endpoint1I_bEndpointAddress;
    u8  endpoint1I_bmAttributes;
    u16 endpoint1I_wMaxPacketSize;
    u8  endpoint1I_bInterval;

    u16 report_UsagePage;
    u16 report_Usage;
    u16 report_Collection;
    u16 report_UsagePage0;
    u16 report_UsageMinimum0;
    u16 report_UsageMaximum0;
    u16 report_LogicalMinimum0;
    u16 report_LogicalMaximum0;
    u16 report_ReportSize0;
    u16 report_ReportCount0;
    u16 report_Input0;
    u16 report_ReportSize1;
    u16 report_ReportCount1;
    u16 report_Input1;
    u16 report_UsagePage2;
    u16 report_UsageMinimum2;
    u16 report_UsageMaximum2;
    u16 report_LogicalMinimum2;
    u16 report_LogicalMaximum2;
    u16 report_ReportSize2;
    u16 report_ReportCount2;
    u16 report_Input2;
    u8  report_EndCollection;

    u8  string0_bLength;
    u8  string0_bDescriptorType;
    u16 string0_wLANGID;

    u8  string1_bLength;
    u8  string1_bDescriptorType;
    u8  string1_bSTRING[20];

    u8  string2_bLength;
    u8  string2_bDescriptorType;
    u8  string2_bSTRING[18];


    u8  string3_bLength;
    u8  string3_bDescriptorType;
    u8  string3_bSTRING[24];

    u8  padding[255];

} __attribute__((packed)) K_DESCRIPTORS;

typedef struct __attribute__((packed)) flashdrive_descriptors {
    u8  device_bLength;
    u8  device_bDescriptorType;
    u16 device_bcdUSB;
    u8  device_bDeviceClass;
    u8  device_bDeviceSubClass;
    u8  device_bDeviceProtocol;
    u8  device_bMaxPacketSize0;
    u16 device_idVendor;
    u16 device_idProduct;
    u16 device_bcdDevice;
    u8  device_iManufacturer;
    u8  device_iProduct;
    u8  device_iSerialNumber;
    u8  device_bNumConfigurations;

    u8  configuration_bLength;
    u8  configuration_bDescriptorType;
    u16 configuration_wTotalLength;
    u8  configuration_bNumInterfaces;
    u8  configuration_bConfigurationValue;
    u8  configuration_iConfiguration;
    u8  configuration_bmAttributes;
    u8  configuration_bMaxPower;

    u8  interface_bLength;
    u8  interface_bDescriptorType;
    u8  interface_bInterfaceNumber;
    u8  interface_bAlternateSetting;
    u8  interface_bNumEndpoints;
    u8  interface_bInterfaceClass;
    u8  interface_bInterfaceSubClass;
    u8  interface_bInterfaceProtocol;
    u8  interface_iInterface;

    u8  endpointIO_bLength;
    u8  endpointIO_bDescriptorType;
    u8  endpointIO_bEndpointAddress;
    u8  endpointIO_bmAttributes;
    u16 endpointIO_wMaxPacketSize;
    u8  endpointIO_bInterval;

    u8  endpoint1I_bLength;
    u8  endpoint1I_bDescriptorType;
    u8  endpoint1I_bEndpointAddress;
    u8  endpoint1I_bmAttributes;
    u16 endpoint1I_wMaxPacketSize;
    u8  endpoint1I_bInterval;

    u8  string0_bLength;
    u8  string0_bDescriptorType;
    u16 string0_wLANGID;

    u8  string1_bLength;
    u8  string1_bDescriptorType;
    u8  string1_bSTRING[20];

    u8  string2_bLength;
    u8  string2_bDescriptorType;
    u8  string2_bSTRING[18];


    u8  string3_bLength;
    u8  string3_bDescriptorType;
    u8  string3_bSTRING[24];

    u8  padding[255];

} __attribute__((packed)) F_DESCRIPTORS;

typedef struct __attribute__((packed)) command_block_wrapper {
    u32 dCBWSignature;
    u32 dCBWTag;
    u32 dCBWDataTransferLength;
    u8  bmCBWFlags;
    struct {
        u8 bCBWLUN : 4;
        u8         : 4;
    };
    struct {
        u8 bCBWCBLength : 5;
        u8              : 3;
    };
    u8  CBWCB[16];
} CBW;

typedef struct __attribute__((packed)) command_status_wrapper {
    u32 dCSWSignature;
    u32 dCSWTag;
    u32 dCSWDataResidue;
    u8  bCSWStatus;
} CSW;

typedef struct __attribute__((packed)) inquiry_data {
    struct {
        u8 peripheral_device_type : 5;
        u8 peripheral_qualifier : 3;
    };
    struct {
        u8 x0 : 7;
        u8 RMB : 1;
    };
    u8 SPC_version;
    struct {
        u8 response_data_format : 4;
        u8 HISUP : 1; 
        u8 NORMACA : 1;
        u8 x1 : 2;
    };
    u8  additional_lenght;
    struct {
        u8 protect : 1;
        u8 x2 : 2;
        u8 threePC : 1;
        u8 TPGS : 2;
        u8 ACC : 1;
        u8 SCCS : 1;
    };
    struct {
        u8 ADDR16 : 1;
        u8 x3 : 2;
        u8 MCHNGR: 1;
        u8 MULTIP : 1;
        u8 VS : 1;
        u8 ENCSERV : 1;
        u8 BQUE : 1;
    };
    struct {
        u8 VS : 1;
        u8 CMDQUE: 1;
        u8 x4 : 1;
        u8 LINKED : 1;
        u8 SYNC : 1;
        u8 WBUS16 : 1;
        u8 x5 : 2;
    };
    u8  T10_vendor_identification_MSB[8];
    u8  product_identification_MSB[16];
    u8  product_revision_level_MSB[4];
} INQUIRY;

typedef struct __attribute__((packed)) sense_data {
    u8  RESPONSE_CODE : 7;
    u8  VALID : 1;
    u8  B1;
    u8  SENSE_KEY : 4;
    u8  x : 1;
    u8  ILI : 1;
    u8  EOM : 1;
    u8  FILEMARK : 1;
    u32 INFORMATION;
    u8  ADDLEN;
    u32 CMDSPINF;
    u8  ASC;
    u8  ASCQ;
    u8  FRUC;
    u8  SENSE_KEY_SPEC[3];
} SENSE;

typedef volatile struct __attribute__((packed)) bulkonly_msc_command_data_status_transfer {
    CSW         csw;
    u32         adress;
    u32         send;
    u8          readwrite;
    u8          dts;
} CDS_TRANSFERT;


void    reset_BD(volatile BD *bdRX);
void    flashdrive_transfert(void);
void    stall(u8 edpt, u8 dir);
void    send_usb (volatile const void *packet, u8 edpt, u32 len, u8 dts);

extern volatile BD          __attribute__((aligned(512))) bdt[8];
extern volatile u8          ep0_Oe[64];
extern volatile u8          ep0_Oo[64];
extern volatile u8          TX0buf[64];
extern volatile u8          TX1buf[64];
extern volatile USTAT       u1stat;
extern const struct __attribute__((packed)) keyboard_descriptors Dkeyboard;
extern const struct __attribute__((packed)) flashdrive_descriptors Dflashdrive;
extern volatile union globales {
    u32 all;
    struct {
        u32 conf : 3;
        u32 control_pending : 3;
        u32 addr : 7;
        u32 even_odd0 : 1;
        u32 even_odd1 : 1;
        u32 dts : 1;
        u32 idlems : 16;
    };
}b;
extern CDS_TRANSFERT bulk;
#endif
