/* 
 * File:   libHS.h
 * Author: nsbr
 *
 * Created on June 27, 2017, 8:25 PM
 */

#ifndef LIBHS_H
#define	LIBHS_H

#include <xc.h>
#include <sys/attribs.h>
#include "datatype.h"

#define SYSCLOCK 80000000
#define PBCLOCK 40000000

void    timer_ms(u32 delay);
void    timer_s(u32 delay);
void    timer_m(u32 delay);
void    xmemset(void *mem, u8 c, u32 n);
s8      xmemcmp(u8 *x1, u8 *x2, u16 n);
void    xmemcpy(void *dst, void *src, u32 n);
s64     xatoi(u8 *nptr);

#endif