/* 
 * File:   HardwareScript.h
 * Author: nsbr
 *
 * Created on May 17, 2017, 10:48 AM
 */

#ifndef HARDWARESCRIPT_H
#define	HARDWARESCRIPT_H

#include <xc.h>
#include "libHS.h"

void                            ini_usb(void);
void                            usb_wake(void);
void                            ini_retry_timer(void);
void                            ini_timer(void);
s8                              ini_sdcard(void);
s8                              sd_readblock(volatile u8 *buf, u32 adress);
s8                              sd_writeblock(u8 *buf, u32 adress, u16 len);
s8                              sd_readmblock(u8 *buf, u32 begin, u8 mode);
void                            keyboard_transfert(void);
s8                              update_keys(u8 stage);
void                            send_cds(const void *packet, u8 mode);

typedef volatile struct                  s_file {
    u8          data[515];
    u32         adress;
    s64         remain;
    u8          sector;
    u32         cluster;
}                               file;

extern volatile struct __attribute__((packed)) keys_report {
    u8  modifiers;
    u8  reserved;
    u8  key0;
    u8  key1;
    u8  key2;
    u8  key3;
    u8  key4;
    u8  key5;
} keys;

typedef struct __attribute__((packed)) jumps {
    file                f;
    u16                 readindex;
    struct keys_report  keys;
    u32                 remain;
} jumps;
#define                 JUMPMAX 32
extern volatile jumps   jump[JUMPMAX];

extern struct read_capacity     capacity;
extern struct __attribute__((packed)) read_capacity {
    u32 lastlba;
    u32 blocklength;
} capacity;
extern volatile file            f0;
extern volatile file            f1;
extern volatile file            *f;
extern volatile u8              key_stage;
extern volatile u16             readindex;
extern volatile u8              jumpindex; 
extern volatile u32             key_interval; 

extern volatile u8              usb_ready;
extern volatile u8              usb_mode;
extern volatile u16             enumusb_retry;

#define LOG_TO_PHY(x) (x & 0x1FFFFFFF)
#define PHY_TO_LOG(x) (x | 0xA0000000)
#define REV_WORD(dword) ((dword & 0xFF000000) >> 24 | (dword & 0x00FF0000) >> 8 | (dword & 0x0000FF00) << 8 | (dword & 0x000000FF) << 24)

#endif
