# Keyscript Specification

## Keys commmands

### Printable characters keys
In a keyscript file, every printable character, except grave accent which isn't between two grave accent is interpreted as a keystroke.  
`` ``` `` Grave accent

### Modifiers keys
##### Identifiers
`LC`  left Control  
`LS`  left Shift  
`LA`  left Alt  
`LG` left Gui  
`RC` right Control  
`RS` right Shift  
`RA` right Alt  
`RG` right Gui  

##### Usage
Hold with  `~` at beginning.  
Release with `~` at end.

##### Exemple
*  `` `~RS` `` Hold right shift
* `` `RS~` `` Release right shift


### Other keys
`` `N` `` Newline
```
`UP`
`DOWN`
`LEFT`
`RIGHT`
`ESC`
`DEL`
`BACK`
`HOME`
`END`
`PGUP`
`PGDWN`
`UNDO`
`COPY`
`PASTE`
`CUT`
`FIND`
`VOLUP`
`VOLDWN`
`PAUSE`
`STOP`
`MUTE`
`POWER`
`LOCK`
`SLOCK`
`NLOCK`
`MENU`
`SELECT`
`PRINTS`
`INSERT`
`EXEC`
`HELP`
`AGAIN`
`F1`
`F2`
`F3`
`F4`
`F5`
`F6`
`F7`
`F8`
`F9`
`F10`
`F11`
`F12`
`F13`
`F14`
`F15`
`F16`
`F17`
`F18`
`F19`
`F20`
`F21`
`F22`
`F23`
`F24`
```


## Special commands

### Interval
`` `*INTERVAL*[uint 32]` ``  

Interval between the keys can be expressed un millisecond with the interval command  
##### Exemple
* `` `*INTERVAL*123` `` for 123 millisecond between keystrokes

### Timers
If the WAIT command is at the beginning of the file, the waiting time occur before registering to the computer. To avoid this behavior, begin by a newline.  
`` `*WAITMS*[uint32]` `` milliseconds  
`` `*WAITS*[uint32]` ``	seconds  
`` `*WAITM*[uint32]` ``	minutes  
##### Exemple
* `` `*WAITS*45` `` wait 45 secondes  

### Loops
`` `*LOOP*[uint32]` `` begin of a loop  
`` `*LOOP*` `` begin of an infinite loop  
`` `*LOOP` `` end of a loop  
If there is no number at the begin of the loop or the number is 0 or uint32 max, the loop is infinite.  
##### Exemple
* `` `*LOOP*8`a`*LOOP` `` send 8 times a key  
* `` `*LOOP*`a`*LOOP` ``send a key constantly  
##### Note
The maximum allowed depth of nested loops is defined by the JUMPMAX macro defined in `HardwareScript.h`.
The default value is 32, it can be changed depending on the available memory.


## Limitation
For now, Keyscript only support qwerty layout.
